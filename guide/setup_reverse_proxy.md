author:            19A ITS1
summary:           Setup a reverse proxy
id:                setup_reverse_proxy
categories:        NGINX reverse proxy
environments:      Virtual machines
status:            wip
feedback link:     MON@UCL.DK
analytics account: Always 0

# Setup a reverse proxy using Nginx

## Introduction

Nginx has the capability of being a reverse proxy. More info in the [official docs](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)

There are two way of doing it

1. by requesting a subdirectory
2. by requesting a host


We will set it up in a vmware environment.

TODO

* Describe some examples so the reader knows what we are talking about.
* This guide should end up in showing how to do both

## Basic design

In order to demonstrate reverse proxying, we need a basic design with multiple internal webservers.

![Design Diagram](img/2019-10-04_-_Exercise_2_Design.png)

TODO

* make a design with a reverse proxy host and some hosts on the inside.
* ensure that we can illustrate both reverse proxy strategies.
* can we make a script the automatically connects to the servers and check that they are working? bash+curl or [pageres-cli](https://github.com/sindresorhus/pageres-cli)?

## Configuring the internal web servers

The internal webserver are basic webservers running nginx.

We will clone them from the basic debian minimal installation

TODO:
* describe the installation procedure (could this be ascript?)
* add files or copy the code needed here
* Describe how to test

## Configuring the reverse proxy

The reverse proxy will have the most complex configuration.

The configuration has two logical parts: the directory and the hostname approches.

TODO:
* describe the installation procedure (could this be ascript?)


### Subdirectory approach

TODO:
* add files or copy the code needed here
* Describe how to test

### Subdirectory approach

TODO:
* add files or copy the code needed here
* Describe how to test

### Convenient links

TODO:
* add link to complete (annotated) config file
* add link to test script

## Automate

TODO:
* we need a script that automatically deploys a vmware VM
* we need a script that provisions the simple web servers
* we need a script that provisions the reverse proxy
* does gitlab CI fit with this somehow?
